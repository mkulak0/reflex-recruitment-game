export class Health {
    value: number = 3;

    handleEvent(event: Event){
        if(event.type === "startGame"){
            this.resetHealth();
            this.updateHealth();
        } else if(event.type === "lostHealth"){
            this.lostHealth();
        }
    }

    lostHealth(){
        if(this.value === 1){
            globalThis.game.eventBus.dispatchEvent(new Event("endGame"));
        }
        this.value--;
        this.updateHealth();
    }

    private updateHealth(){
        let healthDOM = <HTMLSpanElement>document.getElementById("health");
        healthDOM.innerHTML = this.value.toString();
    }

    private resetHealth(){
        this.value = 3;
        this.updateHealth();
    }
}