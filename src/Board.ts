import { Square } from "./Square";
export class Board {

    content: Array<Square> = new Array<Square>();

    size: number = 0;

    internalTimeOut: number = 0;

    get boardDOM(){
        return <HTMLDivElement>document.getElementById("board");
    }

    handleEvent(event: Event){
        if(event.type === "endGame"){
            clearTimeout(this.internalTimeOut);
            this.clearBoard();
        } else if(event.type === "startGame"){
            this.startServingSquares();
        }
    }

    create(boardSize: number){
        this.size = boardSize;
        this.content = [];
        for(let i = 0; i < boardSize; i++){
            for(let j = 0; j < boardSize; j++){
                this.content.push(new Square(i, j));
            }
        }
        this.insertBoard();
    }

    generateHTMLDOM(): HTMLDivElement{
        // That could be less messy
        let container = <HTMLDivElement>document.createElement("div");
        container.classList.add("row", "m-4");
        let cursor: number = 0;
        for(let i = 0; i < this.size; i++){
            let tempRow = <HTMLDivElement>document.createElement("div");
            tempRow.classList.add("d-flex", "flex-row", "justify-content-center");
            for(let j = 0; j < this.size; j++){
                let tempCol = <HTMLDivElement>document.createElement("div");
                tempCol.appendChild(this.content[cursor].generateHTMLDOM())
                tempRow.appendChild(tempCol);
                cursor++;
            }
            container.appendChild(tempRow);
        }
        return container;
    }

    insertBoard(){
        this.clearBoard();
        this.boardDOM.appendChild(this.generateHTMLDOM());
    }

    clearBoard(){
        let tempElement = <HTMLDivElement>document.getElementById("board");
        if(tempElement !== null){
            tempElement.innerHTML = ""
        }
    }

    squareClicked(x: number, y: number){
        let arrayPosition = this.cordsToArrayPosition(x, y);
        if(this.content[arrayPosition].active){
            console.log("You got a point!");
            globalThis.game.points.getPoint();
        } else {
            console.log("Oh no, you lost your health");
            // globalThis.game.health.lostHealth();
            globalThis.game.eventBus.dispatchEvent(new Event("lostHealth"));
        }
        this.content[arrayPosition].active = false;
        this.updateSquare(arrayPosition);
    }

    updateSquare(arrayPosition: number){
        let newDOMElement = this.content[arrayPosition].generateHTMLDOM();
        let oldDOMElement = document.getElementById(`${this.content[arrayPosition].x}${this.content[arrayPosition].y}`);
        oldDOMElement?.parentNode?.replaceChild(newDOMElement, oldDOMElement);
    }

    deactivateSquare(arrayPosition: number){
        this.content[arrayPosition].active = false;
        this.updateSquare(arrayPosition);
        this.startServingSquares();
    }

    startServingSquares(){
        let randomValue = Math.floor(Math.random()*(this.size ** 2));
        this.content[randomValue].active = true;
        this.updateSquare(randomValue);
        this.internalTimeOut = setTimeout(() => {
            this.deactivateSquare(randomValue);
        }, globalThis.game.timer.timeForNextSquareInMiliseconds);
    }

    private cordsToArrayPosition(x: number, y: number){
        return x*this.size + y;
    }
}