export class Timer {
    private time: number = 120;
    private timeForNextSquare: number = 3.6;


    
    public get timeForNextSquareInMiliseconds() : number {
        return Math.floor(this.timeForNextSquare*1000);
    }

    private interval: number = 0;

    private timeout: number = 0;

    handleEvent(event: Event){
        if(event.type === "endGame"){
            clearInterval(this.interval)
            clearTimeout(this.timeout);
        } else if(event.type === "startGame"){
            this.startTimer();
        }
    }

    startTimer(){
        this.time = 120;
        this.timeForNextSquare = 3.6;
        this.changeTime(this.time);
        this.interval = setInterval(() => {
            this.time--;
            if(this.time === 0){
                globalThis.game.eventBus.dispatchEvent(new Event("endGame"));
            }
            this.changeTime(this.time);
            if(this.time % 2 == 0){
                this.timeForNextSquare -= 0.03;
            }
        }, 1000);
        this.timeout = setTimeout(() => clearInterval(this.interval), 2*60*1000+1000);
    }



    private changeTime(newTime: number){
        let timerDOM = <HTMLSpanElement>document.getElementById("time");
        timerDOM.innerHTML = newTime.toString();
    }
}