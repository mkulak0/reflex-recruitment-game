export class Alerts {

    _alertsIncrement: number = 0;

    
    get alertsIncrement() : string {
        this._alertsIncrement++;
        return this._alertsIncrement.toString();
    }
    

    handleEvent(event: Event){
        if(event.type === "lostHealth"){
            this.newAlert("Straciłeś życie!", "danger");
        } else if(event.type === "endGame" && globalThis.game.points.value !== 0){
            this.newAlert("Wynik końcowy: "+globalThis.game.points.value.toString(), "primary");
        }
    }

    newAlert(message: string, type: "primary" | "danger"){
        let newAlert = <HTMLDivElement>document.createElement("div");
        newAlert.classList.add("alert", "alert-"+type);
        newAlert.setAttribute("role", "alert");
        newAlert.innerHTML = message;
        newAlert.id = "alert-"+this.alertsIncrement;
        let alertsContainer = <HTMLDivElement>document.getElementById("alertsContainer");
        alertsContainer.appendChild(newAlert);
        setTimeout(() => {
            alertsContainer.removeChild(newAlert);
        }, 5000);
    }
}