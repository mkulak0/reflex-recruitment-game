export class Points {
    value: number = 0;

    handleEvent(event: Event){
        if(event.type === "startGame"){
            this.resetScore();
        }
    }

    getPoint(){
        this.value++;
        this.updatePoints();
    }

    private updatePoints(){
        let PointsDOM = <HTMLSpanElement>document.getElementById("points");
        PointsDOM.innerHTML = this.value.toString();
    }

    private resetScore(){
        this.value = 0;
        this.updatePoints();
    }
}