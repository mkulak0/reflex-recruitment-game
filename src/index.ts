import { Board } from "./Board";
import { Timer } from "./Timer";
import { Health } from "./Health";
import { Points } from "./Points";
import { Alerts } from "./Alerts";
class Game{

    board: Board = new Board();
    timer: Timer = new Timer();
    health: Health = new Health();
    points: Points = new Points();
    alerts: Alerts = new Alerts();

    eventBus: Comment = new Comment();

    constructor(){
        this.eventBus.addEventListener("endGame", this.board);
        this.eventBus.addEventListener("endGame", this.timer);
        this.eventBus.addEventListener("endGame", this.alerts);
        this.eventBus.addEventListener("startGame", this.timer);
        this.eventBus.addEventListener("startGame", this.points);
        this.eventBus.addEventListener("startGame", this.board);
        this.eventBus.addEventListener("startGame", this.health);
        this.eventBus.addEventListener("lostHealth", this.health);
        this.eventBus.addEventListener("lostHealth", this.alerts);
    }

    start(){
        this.eventBus.dispatchEvent(new Event("endGame"));
        let boardSizeDOM = <HTMLInputElement>document.getElementById("boardSize");
        if(boardSizeDOM !== undefined){
            let boardSize = parseInt(boardSizeDOM.value);
            this.board.create(boardSize);
        }
        this.eventBus.dispatchEvent(new Event("startGame"));
    }
}

declare global {
    var game: Game;
}

globalThis.game = new Game();

let gameButtonDOM = document.getElementById("startButton");

if(gameButtonDOM !== undefined && gameButtonDOM !== null){
    gameButtonDOM.addEventListener("click", () => {
        globalThis.game.start();
    });
}
