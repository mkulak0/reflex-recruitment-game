export class Square {
    active: boolean = false;

    x: number;
    y: number;

    constructor(x: number, y:number){
        this.x = x;
        this.y = y;
    }

    generateHTMLDOM(){
        let tempElement = <HTMLDivElement>document.createElement("div");
        tempElement.onclick = () => {
            globalThis.game.board.squareClicked(this.x, this.y);
        };
        tempElement.classList.add("px-3", "py-1", "m-1");
        tempElement.style.cursor = "pointer";
        if(this.active){
            tempElement.classList.add("bg-success");
        } else {
            tempElement.classList.add("bg-secondary");
        }
        tempElement.appendChild(document.createTextNode("X"));

        tempElement.id = `${this.x}${this.y}`;

        return tempElement;
    }
}